from app import app
from flask_sqlalchemy import SQLAlchemy


app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///database.sqlite3"
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

db = SQLAlchemy(app)


student_course = db.Table(
    "enrollments",
    db.Column("enrollment_id", db.Integer,
              primary_key=True, autoincrement=True),
    db.Column(
        "estudent_id", db.Integer, db.ForeignKey("student.student_id"), nullable=False
    ),
    db.Column(
        "ecourse_id", db.Integer, db.ForeignKey("course.course_id"), nullable=False
    ),
)


class Student(db.Model):
    __tablename__ = "student"

    student_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    roll_number = db.Column(db.String, nullable=False, unique=True)
    first_name = db.Column(db.String, nullable=False)
    last_name = db.Column(db.String)
    courses = db.relationship(
        "Course", secondary=student_course, backref="enrollment")


class Course(db.Model):
    __tablename__ = "course"

    course_id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    course_code = db.Column(db.String, nullable=False, unique=True)
    course_name = db.Column(db.String, nullable=False)
    course_description = db.Column(db.String)
