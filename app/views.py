from app import app
from flask import render_template, request, redirect, url_for
from app.model import Student, Course, db


@app.route("/")
def index():
    students = Student.query.all()
    return render_template("index.html", students=students)


@app.route("/student/<int:id>")
def student_info(id):
    student = Student.query.filter_by(student_id=id).first()
    courses = student.courses
    return render_template("student_details.html", student=student, courses=courses)


@app.route("/student/create", methods=["POST", "GET"])
def add_students():
    courses = Course.query.all()
    if request.method == "POST":
        roll = request.form.get("roll_number")
        fname = request.form.get("first_name")
        lname = request.form.get("last_name")
        courses = request.form.getlist("courses")
        new_student = Student(
            roll_number=roll, first_name=fname, last_name=lname)
        db.session.add(new_student)
        for course in courses:
            # print(Course.query.filter_by(course_id=course).first())
            new_student.courses.append(
                Course.query.filter_by(course_id=course).first())
        db.session.commit()
        return redirect(url_for("index"))
    return render_template("form.html", courses=courses)


@app.route("/student/<int:id>/update", methods=["POST", "GET"])
def update_students(id):
    student = Student.query.filter_by(student_id=id).first()
    courses = Course.query.all()
    if request.method == "POST":
        roll = request.form.get("roll_number")
        fname = request.form.get("first_name")
        lname = request.form.get("last_name")
        courses = request.form.getlist("courses")
        student.roll_number = roll
        student.first_name = fname
        student.last_name = lname
        student.courses.clear()
        for course in courses:
            # print(Course.query.filter_by(course_id=course).first())
            student.courses.append(
                Course.query.filter_by(course_id=course).first())
        db.session.commit()
        return redirect(url_for("index"))
    # print(courses)
    return render_template("form.html", student=student, courses=courses)


@app.route("/student/<int:id>/delete", methods=["POST"])
def delete_student(id):
    student = Student.query.filter_by(student_id=id).first()
    db.session.delete(student)
    db.session.commit()
    return redirect(url_for("index"))
